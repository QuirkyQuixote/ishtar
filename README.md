Ishtar - C++ wrappers for Lua
=============================

A small header-only C++ library to export of C++ objects to Lua.

Requirements
------------

Ishtar

- C++ compiler supporting the C++20 standard
- Lua

Installation
------------

Not strictly necessary since this is a header-only library, but `make install`
will install the header in `$(PREFIX)$(includedir)/ishtar.h` (by default
`$(HOME)/.local/include/ishtar.h`.

You can remove the installed files with `make uninstall`.

You can also run `make check` to compile and run some tests. They require the
`readline` library to be compiled.

Usage
-----

Ishtar is header-only; include `ishtar.h` and bind elements to Lua using
the API:

```
int foo;
int bar(lua_State*);
void baz(const std::string& s);

struct Foo {
        int x;
        int y;
        Foo();
        Foo(int, int);
        Foo& operator+(const Foo&);
        Foo& operator-(const Foo&);
};
```

```cpp
ishtar::globals(l)
        .set("a", true)                         // becomes Lua boolean
        .set("b", 42)                           // becomes Lua integer
        .set("c", 3.14)                         // becomes Lua number
        .set("d", "Hello, World!")              // becomes Lua string
        .set("e", std::make_pair(1, 2))         // becomes Lua table
        .set("f", std::vector<int>{1, 2, 3, 4}) // becomes Lua table

        // becomes Lua table
        .set("my_table", ishtar::table(l)
                .set("foo", foo)                // becomes Lua integer
                .set("bar", bar)                // becomes pure Lua function
                .set("baz", baz)                // becomes wrapped function
            )

        // becomes Lua metatable for type Foo
        .set("Foo", ishtar::type<Foo>(l) 
                .set_constructor<int, int>()    // only one constructor
                .set("x", &T::x)                // export member objects
                .set("y", &T::y)
                .set(ishtar::add, &T::operator+)// export member functions
                .set(ishtar::sub, &T::operator-)
            )
        ;
```

Ishtar will automatically deduce the type of non-overloaded static and member
functions (and static member functions), but will require additional code to
set the type of function objects and lambdas. You can provide the type of these
by wrapping them in the appropriate `std::function`:

```cpp
struct foo {
        int operator(int i) { return i; }
};

auto bar = [](int i){ return i; };

ishtar::Table(l, "name")
        .set("foo", std::function<int(int)>(foo))
        .set("bar", std::function<int(int)>(bar))
        ;
```

Note that not doing this will not fail to export the object; instead, you'll
end with a non-callable object on the Lua side.

Type mappings
-------------

A number of types (mostly from the STL) are mapped between C++ and Lua in a
personalized way:

| C++ types                                                                 | Lua types      |
|---------------------------------------------------------------------------|----------------|
| `bool`                                                                    | boolean        |
| `std::integral`, `std::floating_point`                                    | number         |
| `const char*`, `std::string`, `std::string_view`                          | string         |
| `std::vector`, `std::deque`, `std::list`                                  | table [1]  |
| `std::set`, `std::multiset`, `std::unordered_set`, `std::unordered_multiset` | table [1]  |
| `std::map`, `std::multimap`, `std::unordered_map`, `std::unordered_multimap` | table [2]  |
| `std::pair`, `std::tuple`, `std::array`                                   | table [3] |
| `std::variant`                                                            | varies [4] |
| `std::optional`                                                           | varies [5] |
| `std::bitset`                                                             | number [6] |
| `R(*)(Args...)`, `R(T::*)(Args...)`, `std::function`                      | function |
| `ishtar::Ref`                                                             | any [7]    |
| `ishtar::Skip`                                                            | any [8]    |
| other                                                                     | userdata       |

1. Built from indexed tables.
2. Built from table keys and values.
3. Built from indexed tables when used as function argument; when used as
   function return values they push their contents individually to the stack,
   and the Lua function return more than one value.
4. When used as a return value, pushes to the Lua stack the currently held
   value; when used as a function argument, it becomes the first of the types
   that the `std::variant` can hold for which a transformation is possible.
5. An empty `std::optional` maps to `nil`.
6. If the `std::bitset` does not fit into Lua's numeric representation, there
   may be silent conversion errors. `lua_pushnumber()` requiring a
   floating-point value does not help, either.
7. `ishtar::Ref` references a value in a Lua stack: if it's used as a function
   argument the function is passed a reference to it; if it's used as a return
   value, the value is copied to the stack in the return position. It can be
   used to implement arguments or return values of unspecified type.
8. `ishtar::Skip` is a monostate that can be used to ignore a Lua value.

If a class is not explicitly exported, but it's used as an argument or return
value by a exported function, it will be automatically exported with a mangled
name and no user-accessible constructor the first time it is required.

Function Arguments
------------------

Any callable object other than `lua_CFunction` is exported as a closure where
the function is a wrapper responsible to map Lua arguments to C++ ones, and
call the actual object, that is stored as the first (and only) upvalue.

For every argument that the C++ function requires, the wrapper will look at the
Lua stack. If there's a value that can be mapped in that position, it will
attempt to pop it as the required C++ value. If this is not possible, an error
will be thrown.

Exporting Types
---------------

Types can be exported either explicitly (with the `ishtar::type<T>()` function) or
implicitly (when they're required by the implementation of something else that
has been exported).

### Implicit and Explicit Exporting

All exported types generate a metatable for that particular type and store it
into the register under the name `"ishtar." + typeid(T).name()`. At the very
least, the following metafields will be defined:

- `__name` contains an automatically generated version of the C++ type name
  (see below).
- `__index` contains the metatable itself.
- `__gc` contains a destructor for the type, unless the type is trivially
  destructible, in which case the field is omitted.

### Explicit Exporting

If exported explicitly with `ishtar::type<T>()` the type may be further
developed:

- by using `set_constructor<Args...>()` the metatable becomes callable with the
  addition of a metatable of its own whose `__call` fields will contain a
  wrapper for the constructor `T::T(Args...)`, so an object can be constructed
  by calling the metatable itself.
- metafields may be specified by using the `set(Metamethod, T&&)` function;
  valid metafield are given in the form `ishtar::foo` that generates the
  metafield `__foo`.
- additional fields may be assigned to the metatable with `set(std::string&&,
  T&&)`; these are not set to the table, but to a special calling system that
  allows pointers to member objects and pointers to function objects to be
  handled internally.

### Autogenerated Type Names

When implicitly exporting a type, a name must be generated automatically.
The system attempts to use `typeid(T).name()` that is in some cases a readable
string, and in others somewhat encrypted; for the latter, the exporter checks
for the `cxxabi.h` library, and if present tries to demangle the string with
`abi::__cxa_demangle()`.

