
# Where this file resides

root_dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# When version changes, increment this number

VERSION := 1.0.0

# Determine where we are going to install things

prefix := $(HOME)/.local
includedir := $(prefix)/include

# According to the GNU Make documentation: "Every Makefile should define the
# variable INSTALL, which is the basic command for installing a file into the
# system.  Every Makefile should also define the variables INSTALL_PROGRAM and
# INSTALL_DATA."

INSTALL := install
INSTALL_PROGRAM := $(INSTALL)
INSTALL_DATA := $(INSTALL) -m 644

# Overridable flags

CXXFLAGS ?= -g
CPPFLAGS ?= $(shell pkg-config --cflags lua54-c++ readline)
LDLIBS ?= $(shell pkg-config --libs lua54-c++ readline)

# Override these flags so they can't be re-overriden.

override CXXFLAGS += -std=c++20
override CXXFLAGS += -MMD
override CXXFLAGS += -fPIC
override CXXFLAGS += -Wall
override CXXFLAGS += -Werror
override CXXFLAGS += -Wfatal-errors
override CXXFLAGS += -ftemplate-backtrace-limit=20

override CPPFLAGS += -I$(root_dir)

override LDLIBS += -lstdc++

# Do not allow direct compilation of % from %.cc; it breaks -MMD

%: %.cc

# All tests to be compiled and executed

tests += tests/foo.lua
tests += tests/bar.lua
tests += tests/add.lua
tests += tests/cat.lua
tests += tests/div.lua
tests += tests/move.lua
tests += tests/toggle.lua
tests += tests/maybe_str.lua
tests += tests/byte.lua
tests += tests/accum.lua
tests += tests/uniq.lua
tests += tests/sort.lua
tests += tests/rev.lua
tests += tests/vec.lua
tests += tests/lazy.lua
tests += tests/baz.lua

.PHONY: all
all: tests/shell

.PHONY: check
check: all
	@for i in $(tests); do\
		if tests/shell $$i; then\
			echo "\033[0;32mpass: \033[1;32m$$i\033[0;0m"; \
		else\
			echo "\033[0;31mfail: \033[1;31m$$i\033[0;0m"; \
		fi;\
	done

.PHONY: clean
clean:
	$(RM) tests/shell
	$(RM) tests/*.o
	$(RM) tests/*.d

.PHONY: install
install:
	$(INSTALL_DATA) ishtar.h $(DESTDIR)$(includedir)

.PHONY: uninstall
uninstall:
	-$(RM) $(DESTDIR)$(includedir)/ishtar.h

-include $(shell find -name "*.d")
