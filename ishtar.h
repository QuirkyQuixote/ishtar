// Ishtar - C++ wrappers for Lua

// Copyright (C) 2022-2023 L. Sanz <luis.sanz@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


#ifndef _ISHTAR_H
#define _ISHTAR_H

#include <filesystem>
#include <vector>
#include <deque>
#include <list>
#include <set>
#include <unordered_set>
#include <map>
#include <unordered_map>
#include <tuple>
#include <ranges>
#include <functional>
#include <variant>
#include <optional>
#include <bitset>
#include <typeinfo>

#if defined(__has_include)
#       if __has_include(<cxxabi.h>)
#               include <cxxabi.h>
#               define _ISHTAR_HAS_CXXABI_H
#       endif
#endif

#include "lua.hpp"

namespace ishtar {

// Forward declarations

template<class T> bool is(lua_State* l, int i);
template<class T> T to(lua_State* l, int i);
template<class T, class U = std::remove_cvref_t<std::decay_t<T>>>
        int push(lua_State* l, const T& t);
template<class T, class U = std::remove_cvref_t<std::decay_t<T>>>
        int push(lua_State* l, T&& t);

// Any Lua value can be cast to a Skip value; used mostly when the to()
// function must ignore a value of any type.

enum class Skip{};

// Reference to a Lua value: operates like an unique pointer; multiple
// references to the same Lua value may exist, but they are not copies of the
// same reference.

class Ref {
 private:
        lua_State* l{nullptr};
        int i{LUA_NOREF};

 public:
        Ref() = default;
        Ref(const Ref& r) = delete;
        Ref(Ref&& r) { swap(*this, r); }

        Ref(lua_State* l, int n) : l{l}
        {
                lua_pushvalue(l, n);
                i = luaL_ref(l, LUA_REGISTRYINDEX);
        }

        ~Ref()
        {
                if (i != LUA_NOREF)
                        luaL_unref(l, LUA_REGISTRYINDEX, i);
        }

        Ref& operator=(const Ref& r) = delete;
        Ref& operator=(Ref&& r) { swap(*this, r); return *this; }

        constexpr operator bool() const { return i != LUA_NOREF; }
        constexpr bool operator!() const { return i == LUA_NOREF; }
        constexpr lua_State* state() const { return l; }
        constexpr int index() const { return i; }
        constexpr void release() { i = LUA_NOREF; }

        friend constexpr void swap(Ref& x, Ref& y)
        {
                std::swap(x.l, y.l);
                std::swap(x.i, y.i);
        }
};

// Interface for reference to a Lua object using CRTP

template<class Impl> struct Object { };

// Wrapper for a table reference that provides a set() function

struct Table : public Object<Table> {
 private:
        Ref r;

 public:
        Table() = default;
        explicit Table(Ref&& r) : r{std::move(r)} {}

        constexpr const Ref& ref() const { return r; }

        template<class T> Table&& set(std::string&& k, T&& t)
        {
                push(r.state(), r);
                push(r.state(), std::move(k));
                push(r.state(), std::forward<T>(t));
                lua_settable(r.state(), -3);
                lua_pop(r.state(), 1);
                return std::move(*this);
        }
};

inline Table table(lua_State* l)
{
        lua_newtable(l);
        Ref r{l, -1};
        lua_pop(l, 1);
        return Table{std::move(r)};
}

inline Table globals(lua_State* l)
{
        lua_pushglobaltable(l);
        Ref r{l, -1};
        lua_pop(l, -1);
        return Table{std::move(r)};
}

// Wrapper around abi::__cxa_demangle() to get the "real" name of types

#ifdef _ISHTAR_HAS_CXXABI_H
static inline std::string demangle(const char* name)
{
        if (char* realname = abi::__cxa_demangle(name, 0, 0, 0)) {
                std::string solution{realname};
                free(realname);
                return solution;
        }
        return "";
}
#else
static inline std::string demangle(const char* name) { return name; }
#endif

template<class T> const char* type_name()
{
        static std::string instance = demangle(typeid(T).name());
        return instance.c_str();
}

// Concepts required to implement the to() and push() functions.

template<class T> struct is_std_tuple : public std::bool_constant<false> {};
template<class A, class B> struct is_std_tuple<std::pair<A, B>> : public std::bool_constant<true> {};
template<class... T> struct is_std_tuple<std::tuple<T...>> : public std::bool_constant<true> {};
template<class T, size_t N> struct is_std_tuple<std::array<T, N>> : public std::bool_constant<true> {};
template<class T> concept std_tuple = is_std_tuple<T>::value;

template<class T> struct is_std_string : public std::bool_constant<false> {};
template<class... T> struct is_std_string<std::basic_string<T...>> : public std::bool_constant<true> {};
template<class... T> struct is_std_string<std::basic_string_view<T...>> : public std::bool_constant<true> {};
template<class T> concept std_string = is_std_string<T>::value;

template<class T> struct is_std_sequence : public std::bool_constant<false> {};
template<class... T> struct is_std_sequence<std::vector<T...>> : public std::bool_constant<true> {};
template<class... T> struct is_std_sequence<std::deque<T...>> : public std::bool_constant<true> {};
template<class... T> struct is_std_sequence<std::list<T...>> : public std::bool_constant<true> {};
template<class T> concept std_sequence = is_std_sequence<T>::value;

template<class T> struct is_std_set : public std::bool_constant<false> {};
template<class... T> struct is_std_set<std::set<T...>> : public std::bool_constant<true> {};
template<class... T> struct is_std_set<std::multiset<T...>> : public std::bool_constant<true> {};
template<class... T> struct is_std_set<std::unordered_set<T...>> : public std::bool_constant<true> {};
template<class... T> struct is_std_set<std::unordered_multiset<T...>> : public std::bool_constant<true> {};
template<class T> concept std_set = is_std_set<T>::value;

template<class T> struct is_std_map : public std::bool_constant<false> {};
template<class... T> struct is_std_map<std::map<T...>> : public std::bool_constant<true> {};
template<class... T> struct is_std_map<std::multimap<T...>> : public std::bool_constant<true> {};
template<class... T> struct is_std_map<std::unordered_map<T...>> : public std::bool_constant<true> {};
template<class... T> struct is_std_map<std::unordered_multimap<T...>> : public std::bool_constant<true> {};
template<class T> concept std_map = is_std_map<T>::value;

template<class T> struct is_std_variant : public std::bool_constant<false> {};
template<class... T> struct is_std_variant<std::variant<T...>> : public std::bool_constant<true> {};
template<class T> concept std_variant = is_std_variant<T>::value;

template<class T> struct is_std_optional : public std::bool_constant<false> {};
template<class T> struct is_std_optional<std::optional<T>> : public std::bool_constant<true> {};
template<class T> concept std_optional = is_std_optional<T>::value;

template<class T> struct is_std_bitset : public std::bool_constant<false> {};
template<size_t N> struct is_std_bitset<std::bitset<N>> : public std::bool_constant<true> {};
template<class T> concept std_bitset = is_std_bitset<T>::value;

// Wraps a C++ function in a Lua closure consisting on a lua_CFunction with an
// upvalue that refers to the actual C++ function.
//
// - Function arguments and return values are automatically handled by calls to
//   to() and push() respectively
// - Exceptions are caught and transformed to calls to luaL_error().
// - The argument number for the Lua call must match the number of arguments of
//   the C++ function.
// - There's no handling for variadic functions or default arguments.

template<class T, class... Args> int call_impl(lua_State* l, T& t)
requires (!std::same_as<std::invoke_result_t<T, Args...>, void>)
{
        return [l, &t]<size_t... I>(std::index_sequence<I...>)
        { return push(l, std::invoke(t, to<std::decay_t<Args>>(l, int(I) + 1)...)); }
        (std::make_index_sequence<sizeof...(Args)>{});
}

template<class T, class... Args> int call_impl(lua_State* l, T& t)
requires std::same_as<std::invoke_result_t<T, Args...>, void>
{
        return [l, &t]<size_t... I>(std::index_sequence<I...>)
        { std::invoke(t, to<std::decay_t<Args>>(l, int(I) + 1)...); return 0; }
        (std::make_index_sequence<sizeof...(Args)>{});
}

template<class T, class... Args> int call_function(lua_State* l)
{
        if (lua_gettop(l) != sizeof...(Args))
                return luaL_error(l, "expected %d arguments", sizeof...(Args));
        try {
                T& t = *reinterpret_cast<T*>(lua_touserdata(l, lua_upvalueindex(1)));
                return call_impl<T, Args...>(l, t);
        } catch (std::exception& ex) {
                return luaL_error(l, ex.what());
        }
}

template<class T, class... Args> int call_constructor(lua_State* l)
{
        if (lua_gettop(l) != sizeof...(Args) + 1)
                return luaL_error(l, "expected %d arguments", sizeof...(Args));
        try {
                return [l]<size_t... I>(std::index_sequence<I...>)
                { return push(l, T{to<Args>(l, int(I) + 2)...}); }
                (std::make_index_sequence<sizeof...(Args)>{});
        } catch (std::exception& ex) {
                return luaL_error(l, ex.what());
        }
}

// Constructs a metatable to map C++ classes to Lua ones

struct Field_base {
        virtual ~Field_base() = default;
        virtual int index(lua_State* l) = 0;
        virtual void newindex(lua_State* l) = 0;
};

template<class T, class U> struct Field : public Field_base {
        U data;
        explicit Field(U&& u) : data{std::forward<U>(u)} {}
        ~Field() = default;

        int index(lua_State* l)
        {
                T& t = *reinterpret_cast<T*>(lua_touserdata(l, 1));
                if constexpr (std::is_member_object_pointer<U>::value)
                        if constexpr (std::is_pointer<T>::value)
                                return ishtar::push(l, t->*data);
                        else
                                return ishtar::push(l, t.*data);
                else if constexpr (std::is_member_function_pointer<U>::value)
                        return ishtar::push(l, data);
                else
                        return ishtar::push(l, data);
        }

        void newindex(lua_State* l)
        {
                T& t = *reinterpret_cast<T*>(lua_touserdata(l, 1));
                if constexpr (std::is_member_object_pointer<U>::value)
                        if constexpr (std::is_pointer<T>::value)
                                t->*data = to<std::decay_t<decltype(t->*data)>>(l, 3);
                        else
                                t.*data = to<std::decay_t<decltype(t.*data)>>(l, 3);
                else if constexpr (std::is_member_function_pointer<U>::value)
                        throw std::runtime_error{"attempt to overwrite member function"};
                else if constexpr (std::is_function<std::remove_pointer_t<U>>::value)
                        throw std::runtime_error{"attempt to overwrite member function"};
                else
                        data = to<U>(l, 3);
        }
};

struct Metamethod {
        const char* name;
};

inline constexpr Metamethod add{"__add"};
inline constexpr Metamethod sub{"__sub"};
inline constexpr Metamethod mul{"__mul"};
inline constexpr Metamethod div{"__div"};
inline constexpr Metamethod mod{"__mod"};
inline constexpr Metamethod pow{"__pow"};
inline constexpr Metamethod unm{"__unm"};
inline constexpr Metamethod idiv{"__idiv"};
inline constexpr Metamethod band{"__band"};
inline constexpr Metamethod bor{"__bor"};
inline constexpr Metamethod bxor{"__bxor"};
inline constexpr Metamethod bnot{"__bnot"};
inline constexpr Metamethod shl{"__shl"};
inline constexpr Metamethod shr{"__shr"};
inline constexpr Metamethod concat{"__concat"};
inline constexpr Metamethod len{"__len"};
inline constexpr Metamethod eq{"__eq"};
inline constexpr Metamethod lt{"__lt"};
inline constexpr Metamethod le{"__le"};
inline constexpr Metamethod call{"__call"};
inline constexpr Metamethod name{"__name"};

template<class T> class Metatable {
 private:
        static int _destroy(lua_State* l)
        {
                reinterpret_cast<T*>(lua_touserdata(l, 1))->~T();
                return 0;
        }

        static int _index(lua_State* l)
        {
                try {
                        return fields().at(lua_tostring(l, 2))->index(l);
                } catch (std::exception& ex) {
                        return luaL_error(l, ex.what());
                }
        }

        static int _newindex(lua_State* l)
        {
                try {
                        fields().at(lua_tostring(l, 2))->newindex(l);
                        return 0;
                } catch (std::exception& ex) {
                        return luaL_error(l, ex.what());
                }
        }

 public:
        static auto& fields()
        {
                static std::unordered_map<std::string, std::unique_ptr<Field_base>> instance;
                return instance;
        }

        static const char* name(lua_State* l)
        {
                if (luaL_getmetatable(l, type_name<T>()) == LUA_TTABLE) {
                        lua_getfield(l, -1, "__name");
                        const char* r = lua_tostring(l, -1);
                        lua_pop(l, 2);
                        return r;
                }
                lua_pop(l, 1);
                return type_name<T>();
        }

        static void push(lua_State* l)
        {
                if (luaL_newmetatable(l, type_name<T>())) {
                        lua_pushcfunction(l, _index);
                        lua_setfield(l, -2, "__index");
                        lua_pushcfunction(l, _newindex);
                        lua_setfield(l, -2, "__newindex");
                        if constexpr (!std::is_trivially_destructible_v<T>) {
                                lua_pushcfunction(l, _destroy);
                                lua_setfield(l, -2, "__gc");
                        }
                }
        }
};

template<class T> class Type : public Object<Type<T>> {
 private:
        Ref r;

 public:
        Type() = default;

        explicit Type(lua_State* l)
        {
                Metatable<T>::push(l);
                r = Ref{l, -1};
                lua_pop(l, 1);
        }

        constexpr const Ref& ref() const { return r; }

        template<class U> Type&& set(std::string&& k, U&& u)
        {
                using B = std::remove_cvref_t<std::decay_t<U>>;
                auto f = new Field<T, B>{std::forward<U>(u)};
                Metatable<T>::fields().emplace(std::move(k), std::unique_ptr<Field_base>(f));
                return std::move(*this);
        }

        template<class U> Type&& set(Metamethod mm, U&& u)
        {
                push(r.state(), r);
                ishtar::push(r.state(), std::forward<U>(u));
                lua_setfield(r.state(), -2, mm.name);
                lua_pop(r.state(), 1);
                return std::move(*this);
        }

        template<class... Args> Type&& set_constructor()
        {
                push(r.state(), r);
                lua_newtable(r.state());
                lua_pushcclosure(r.state(), call_constructor<T, Args...>, 0);
                lua_setfield(r.state(), -2, "__call");
                lua_setmetatable(r.state(), -2);
                lua_pop(r.state(), 1);
                return std::move(*this);
        }
};

template<class T> Type<T> type(lua_State* l) { return Type<T>{l}; }

// Type traits

template<class T> struct type_traits {
        static bool is(lua_State* l, int i)
        {
                i = lua_absindex(l, i);
                if (!lua_getmetatable(l, i)) return 0;
                Metatable<T>::push(l);
                auto r = lua_rawequal(l, -1, -2);
                lua_pop(l, 2);
                return r;
        }

        static T to(lua_State* l, int i)
        {
                i = lua_absindex(l, i);
                if (!lua_getmetatable(l, i))
                        throw std::runtime_error{std::string("expected ") + Metatable<T>::name(l)};
                Metatable<T>::push(l);
                bool r = lua_rawequal(l, -1, -2);
                lua_pop(l, 2);
                if (!r) throw std::runtime_error{std::string("expected ") + Metatable<T>::name(l)};
                if constexpr (std::copy_constructible<T>)
                        return *reinterpret_cast<T*>(lua_touserdata(l, i));
                else
                        return std::move(*reinterpret_cast<T*>(lua_touserdata(l, i)));
        }

        static int push(lua_State* l, const T& t)
        {
                new(lua_newuserdata(l, sizeof(T))) T(t);
                Metatable<T>::push(l);
                lua_setmetatable(l, -2);
                return 1;
        }

        static int push(lua_State* l, T&& t)
        {
                new(lua_newuserdata(l, sizeof(T))) T(std::move(t));
                Metatable<T>::push(l);
                lua_setmetatable(l, -2);
                return 1;
        }
};

// Type traits for boolean values
//
// Any Lua value may be cast to a boolean: false and nil will become false,
// everything else will become true.

template<> struct type_traits<bool> {
        static bool is(lua_State* l, int i) { return true; }
        static bool to(lua_State* l, int i) { return lua_toboolean(l, i); }
        static int push(lua_State* l, bool b) { lua_pushboolean(l, b); return 1; }
};

// Type traits for integral values - just a wrapper for Lua functions

template<std::integral T> struct type_traits<T> {
        static bool is(lua_State* l, int i) { return lua_isinteger(l, i); }
        static T to(lua_State* l, int i)
        {
                if (lua_isinteger(l, i)) return lua_tointeger(l, i);
                throw std::runtime_error{"expected integer"};
        }
        static int push(lua_State* l, T t)
        { lua_pushinteger(l, t); return 1; }
};

// Type traits for real values - just a wrapper for Lua functions

template<std::floating_point T> struct type_traits<T> {
        static bool is(lua_State* l, int i) { return lua_isnumber(l, i); }
        static T to(lua_State* l, int i)
        {
                if (lua_isnumber(l, i)) return lua_tonumber(l, i);
                throw std::runtime_error{"expected number"};
        }
        static int push(lua_State* l, T t)
        { lua_pushnumber(l, t); return 1; }
};

// Type traits for string values
//
// C strings are just a wrapper for Lua string handling
// STL string containers have custom code that don't require the string to be
// null-terminated.

template<> struct type_traits<char*> {
        static bool is(lua_State* l, int i) { return lua_isstring(l, i); }
        static const char* to(lua_State* l, int i)
        {
                if (lua_isstring(l, i)) return lua_tostring(l, i);
                throw std::runtime_error{"expected string"};
        }
        static int push(lua_State* l, const char* t)
        { lua_pushstring(l, t); return 1; }
};

template<std_string T> struct type_traits<T> {
        static bool is(lua_State* l, int i) { return lua_isstring(l, i); }

        static T to(lua_State* l, int i)
        {
                if (lua_isstring(l, i)) return lua_tostring(l, i);
                throw std::runtime_error{"expected string"};
        }

        static int push(lua_State* l, T&& t)
        { lua_pushlstring(l, t.data(), t.size()); return 1; }
};

// Type traits for STL tuples
//
// Tuple types are cast from tables of the right size; ideally they should be
// actual Lua sequences, but there's no simple way to check of a Lua table is
// an actual sequence.

template<std_tuple T> struct type_traits<T> {
        static bool is(lua_State* l, int i)
        {
                return lua_istable(l, i) && luaL_len(l, i) == std::tuple_size_v<T>;
        }

        static T to(lua_State* l, int i)
        {
                i = lua_absindex(l, i);
                if (!is(l, i))
                        throw std::runtime_error{"expected " +
                                std::to_string(std::tuple_size_v<T>) + "-tuple"};
                return [l, i]<size_t... I>(std::index_sequence<I...>)
                {
                        (lua_geti(l, i, int(std::tuple_size_v<T> - I)),...);
                        T t{ishtar::to<std::tuple_element_t<I, T>>(l, -(int(I) + 1))...};
                        lua_pop(l, int(std::tuple_size_v<T>));
                        return t;
                }
                (std::make_index_sequence<std::tuple_size_v<T>>{});
        }

        static int push(lua_State* l, T&& t)
        {
                lua_createtable(l, std::tuple_size_v<T>, 0);
                [l, &t]<size_t... I>(std::index_sequence<I...>)
                { (ishtar::push(l, std::move(std::get<I>(t))),...); }
                (std::make_index_sequence<std::tuple_size_v<T>>{});
                return std::tuple_size_v<T>;
        }
};

// Type traits for STL sequences
//
// Sequence types are cast from tables of size; ideally they should be actual
// Lua sequences, but there's no simple way to check of a Lua table is an
// actual sequence.

template<std_sequence T> struct type_traits<T> {
        static bool is(lua_State* l, int i) { return lua_istable(l, i); }

        static T to(lua_State* l, int i)
        {
                i = lua_absindex(l, i);
                if (!is(l, i))
                        throw std::runtime_error{"expected sequence"};
                T t;
                for (int n = 1; n <= luaL_len(l, i); ++n) {
                        lua_pushinteger(l, n);
                        lua_gettable(l, i);
                        t.push_back(ishtar::to<typename T::value_type>(l, -1));
                }
                return t;
        }

        static int push(lua_State* l, T&& t)
        {
                lua_newtable(l);
                int n = 1;
                for (auto& x : t) {
                        lua_pushinteger(l, n++);
                        ishtar::push(l, std::move(x));
                        lua_settable(l, -3);
                }
                return 1;
        }
};

// Type traits for STL sets
//
// Set types are cast from tables of size; ideally they should be actual
// Lua sequences, but there's no simple way to check of a Lua table is an
// actual sequence.

template<std_set T> struct type_traits<T> {
        static bool is(lua_State* l, int i) { return lua_istable(l, i); }

        static T to(lua_State* l, int i)
        {
                i = lua_absindex(l, i);
                if (!is(l, i))
                        throw std::runtime_error{"expected set"};
                T t;
                for (int n = 1; n <= luaL_len(l, i); ++n) {
                        lua_pushinteger(l, n);
                        lua_gettable(l, i);
                        t.insert(ishtar::to<typename T::value_type>(l, -1));
                }
                return t;
        }

        static int push(lua_State* l, T&& t)
        {
                lua_newtable(l);
                int n = 1;
                for (auto& x : t) {
                        lua_pushinteger(l, n++);
                        ishtar::push(l, std::move(x));
                        lua_settable(l, -3);
                }
                return 1;
        }
};

// Type traits for STL maps
//
// Map types are cast from tables of size: all keys and values will be mapped
// into keys and values.

template<std_map T> struct type_traits<T> {
        static bool is(lua_State* l, int i) { return lua_istable(l, i); }

        static T to(lua_State* l, int i)
        {
                i = lua_absindex(l, i);
                if (!is(l, i))
                        throw std::runtime_error{"expected map"};
                T t;
                lua_pushnil(l);
                while (lua_next(l, i))
                        t.emplace(ishtar::to<typename T::key_type>(l, -2),
                                        ishtar::to<typename T::mapped_type>(l, -1));
                return t;
        }

        static int push(lua_State* l, T&& t)
        {
                lua_newtable(l);
                for (auto& x : t) {
                        ishtar::push(l, std::move(x.first));
                        ishtar::push(l, std::move(x.second));
                        lua_settable(l, -3);
                }
                return 1;
        }
};

// Type traits for STL variants
//
// When pushing, delegates in the traits for the actual type held by the
// variant; when casting, it will take a value of the first type T for which
// the type_traits<T>::is() function returns true, or throw an exception
// otherwise.

template<std_variant T> struct type_traits<T> {
        static bool is(lua_State* l, int i)
        {
                i = lua_absindex(l, i);
                return [l, i]<size_t... I>(std::index_sequence<I...>)
                { return (ishtar::is<std::variant_alternative_t<I, T>>(l, i) || ...); }
                (std::make_index_sequence<std::variant_size_v<T>>{});
        }

        template<size_t I> static T to_impl(lua_State* l, int i)
        {
                if constexpr (I == std::variant_size_v<T>) {
                        throw std::runtime_error{"expected variant"};
                } else {
                        using U = std::variant_alternative_t<I, T>;
                        if (ishtar::is<U>(l, i)) return ishtar::to<U>(l, i);
                        return to_impl<I + 1>(l, i);
                }
        }

        static T to(lua_State* l, int i)
        {
                i = lua_absindex(l, i);
                return to_impl<0>(l, i);
        }

        static int push(lua_State* l, T&& t)
        {
                return std::visit([l](auto&& u){ return ishtar::push(l, std::move(u)); }, t);
        }
};

// Type traits for STL optionals
//
// When pushing, it will delegate on the held type, or push nil; when casting,
// it will construct an empty optional for nil, and delegate on the held type
// otherwise.

template<std_optional T> struct type_traits<T> {
        static bool is(lua_State* l, int i)
        {
                return lua_isnil(l, i) || ishtar::is<typename T::value_type>(l, i);
        }

        static T to(lua_State* l, int i)
        {
                i = lua_absindex(l, i);
                return lua_isnil(l, i) ? T{} : T{ishtar::to<typename T::value_type>(l, i)};
        }

        static int push(lua_State* l, T&& t)
        {
                if (t) return ishtar::push(l, std::move(t.value()));
                lua_pushnil(l); return 1;
        }
};

// Type traits for STL bitsets - maps to Lua integers

template<std_bitset T> struct type_traits<T> {
        static bool is(lua_State* l, int i)
        {
                return lua_isinteger(l, i);
        }

        static T to(lua_State* l, int i)
        {
                if (lua_isinteger(l, i)) return T(lua_tointeger(l, i));
                throw std::runtime_error{"expected bitset"};
        }

        static int push(lua_State* l, T&& t)
        {
                lua_pushinteger(l, t.to_ulong()); return 1;
        }
};

// Type traits for lua_CFunction

template<> struct type_traits<lua_CFunction> {
        static bool is(lua_State* l, int i)
        {
                return lua_iscfunction(l, i);
        }

        static lua_CFunction to(lua_State* l, int i)
        {
                if (is(l, i)) return lua_tocfunction(l, i);
                throw std::runtime_error{"expected function"};
        }

        static int push(lua_State* l, lua_CFunction t)
        {
                lua_pushcfunction(l, t); return 1;
        }
};

// Returns the actual type of the function that can be built from specific
// callable types:
//
// - pointer to functions have their pointer and qualifier removed
// - pointer to member function have the object set as first argument
// - std::function<T> instances become T

template<class T> struct func_base { };

template<class R, class... A> struct func_base<R(*)(A...)> { using type = R(A...); };
template<class R, class... A> struct func_base<R(*)(A...) noexcept> { using type = R(A...); };
template<class R, class C, class... A> struct func_base<R(C::*)(A...)> { using type = R(C, A...); };
template<class R, class C, class... A> struct func_base<R(C::*)(A...) const> { using type = R(const C&, A...); };
template<class R, class C, class... A> struct func_base<R(C::*)(A...) volatile> { using type = R(const C&, A...); };
template<class R, class C, class... A> struct func_base<R(C::*)(A...) const volatile> { using type = R(const C&, A...); };
template<class R, class C, class... A> struct func_base<R(C::*)(A...) noexcept> { using type = R(C, A...); };
template<class R, class C, class... A> struct func_base<R(C::*)(A...) const noexcept> { using type = R(const C&, A...); };
template<class R, class C, class... A> struct func_base<R(C::*)(A...) volatile noexcept> { using type = R(const C&, A...); };
template<class R, class C, class... A> struct func_base<R(C::*)(A...) const volatile noexcept> { using type = R(const C&, A...); };
template<class T> struct func_base<std::function<T>> { using type = T; };

template<class T> using func_base_t = typename func_base<T>::type;

// For the purposes of this library, a function is a type for which a base
// function type can be extracted with func_base_t

template<class T> concept function = requires { typename func_base<T>::type; };

// Helper class to find the call_function<...>() to be called for a function
// and its base function type

template<class T, class F> struct func_traits {};

template<class T, class R, class... A> struct func_traits<T, R(A...)> {
        static int call(lua_State* l)
        { return call_function<T, A...>(l); }
};

// Type traits for functions

template<function T> struct type_traits<T> {
        static int push(lua_State* l, T&& t)
        {
                new(lua_newuserdata(l, sizeof(T))) T(std::forward<T>(t));
                lua_pushcclosure(l, func_traits<T, func_base_t<T>>::call, 1);
                return 1;
        }
};

// Type traits for Objects

template<> struct type_traits<Table> {
        static int push(lua_State* l, Table&& t)
        {
                return ishtar::push(l, t.ref());
        }
};

template<class T> struct type_traits<Type<T>> {
        static int push(lua_State* l, Type<T>&& t)
        {
                return ishtar::push(l, t.ref());
        }
};

// Type traits for Ref

template<> struct type_traits<Ref> {
        static bool is(lua_State* l, int i) { return true; }

        static Ref to(lua_State* l, int i)
        {
                return Ref{l, i};
        }

        static int push(lua_State* l, const Ref& t)
        {
                lua_pushinteger(l, t.index());
                lua_gettable(l, LUA_REGISTRYINDEX);
                return 1;
        }
};

// Actual implementations for is(), to(), push()

template<class T> bool is(lua_State* l, int i)
{
        return type_traits<T>::is(l, i);
}

template<class T> T to(lua_State* l, int i)
{
        return type_traits<T>::to(l, i);
}

template<class T, class U> int push(lua_State* l, const T& t)
{
        return type_traits<U>::push(l, t);
}

template<class T, class U> int push(lua_State* l, T&& t)
{
        return type_traits<U>::push(l, std::move(t));
}

// Serialization of Lua values

template<size_t Depth> std::ostream& print(std::ostream& os, lua_State* l, int i)
{
        static constexpr const char* type = "\e[0;34m";
        static constexpr const char* data = "\e[0;36m";
        static constexpr const char* end = "\e[0;0m";
        static constexpr const char* lbrace = "\e[0;33m{";
        static constexpr const char* rbrace = "\e[0;33m}";
        static constexpr const char* comma = "\e[0;0m,";
        static constexpr const char* equal = "\e[0;0m=";
        static constexpr const char* colon = "\e[0;0m:";
        static constexpr const char* ellipsis = "\e[0;0m...";

        switch (lua_type(l, i)) {
         case LUA_TNONE:
                return os << data << "?" << end;
         case LUA_TNIL:
                return os << data << "nil" << end;
         case LUA_TNUMBER:
                return os << data << lua_tonumber(l, i) << end;
         case LUA_TBOOLEAN:
                return os << data << (lua_toboolean(l, i) ? "true" : "false") << end;
         case LUA_TSTRING:
                return os << data << '"' << lua_tostring(l, i) << '"' << end;
         case LUA_TTABLE:
                if constexpr (Depth > 0) {
                        os << lbrace;
                        lua_pushnil(l);
                        bool is_first = true;
                        i = lua_absindex(l, i);
                        while (lua_next(l, i)) {
                                if (is_first) is_first = false;
                                else os << comma;
                                print<Depth - 1>(os, l, -2);
                                os << equal;
                                print<Depth - 1>(os, l, -1);
                                lua_pop(l, 1);
                        }
                        return os << rbrace << end;
                }
                return os << lbrace << ellipsis << rbrace << end;
         case LUA_TFUNCTION:
                return os << type << "F" << colon << data << lua_topointer(l, i) << end;
         case LUA_TUSERDATA:
                return os << type << "U" << colon << data << lua_topointer(l, i) << end;
         case LUA_TTHREAD:
                return os << type << "T" << colon << data << lua_topointer(l, i) << end;
         case LUA_TLIGHTUSERDATA:
                return os << type << "L" << colon << data << lua_topointer(l, i) << end;
         default:
                throw std::bad_cast{};
        }
}

inline void propagate_error(lua_State* state)
{
        std::string message = lua_tostring(state, -1);
        lua_pop(state, 1);
        throw std::runtime_error{message};
}

inline void run(lua_State* state, const std::filesystem::path& path)
{
        if (luaL_loadfile(state, path.string().c_str()) != LUA_OK ||
                        lua_pcall(state, 0, LUA_MULTRET, 0) != LUA_OK)
                propagate_error(state);
}

inline void run(lua_State* state, const char* string)
{
        if (luaL_loadstring(state, string) != LUA_OK ||
                        lua_pcall(state, 0, LUA_MULTRET, 0) != LUA_OK)
                propagate_error(state);
}

inline void run(lua_State* state, const std::string& string)
{ run(state, string.c_str()); }

inline void run(lua_State* state, std::string_view string)
{ run(state, std::string(string)); }

inline void run(lua_State* state, std::istream& stream)
{
        std::istreambuf_iterator<char> begin(stream), end;
        std::string string(begin, end);
        ishtar::run(state, string);
}

};

inline std::ostream& operator<<(std::ostream& os, const std::pair<lua_State*, int>& r)
{ return ishtar::print<1>(os, r.first, r.second); }

#endif // _ISHTAR_H
