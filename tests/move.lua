
if (nil ~= it.move(nil)) then error("bad move!") end
if (true ~= it.move(true)) then error("bad move!") end
if (10 ~= it.move(10)) then error("bad move!") end
if (3.141592 ~= it.move(3.141592)) then error("bad move!") end
if ("ten" ~= it.move("ten")) then error("bad move!") end
if (it.move ~= it.move(it.move)) then error("bad move!") end
