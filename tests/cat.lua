if (it.cat("", "") ~= "") then error("bad cat!") end
if (it.cat("foo", "bar") ~= "foobar") then error("bad cat!") end
if (it.cat("hello, ", "world!") ~= "hello, world!") then error("bad cat!") end
if (it.cat("my back ", "hurts") ~= "my back hurts") then error("bad cat!") end
