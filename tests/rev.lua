if (not it.equal(it.rev({}), {})) then error() end
if (not it.equal(it.rev({1}), {1})) then error() end
if (not it.equal(it.rev({1, 2}), {2, 1})) then error() end
if (not it.equal(it.rev({2, 1}), {1, 2})) then error() end
