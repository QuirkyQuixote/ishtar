if (it.accum({}) ~= 0) then error("Bad sum!") end
if (it.accum({1}) ~= 1) then error("Bad sum!") end
if (it.accum({1,2}) ~= 3) then error("Bad sum!") end
if (it.accum({1,2,3}) ~= 6) then error("Bad sum!") end
if (it.accum({1,2,3,4}) ~= 10) then error("Bad sum!") end
