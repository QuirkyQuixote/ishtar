if (not it.equal(it.sort({}), {})) then error() end
if (not it.equal(it.sort({1}), {1})) then error() end
if (not it.equal(it.sort({1, 2}), {1, 2})) then error() end
if (not it.equal(it.sort({2, 1}), {1, 2})) then error() end
