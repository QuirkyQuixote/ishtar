
#include <iostream>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "testlib.h"
#include "readline/readline.h"
#include "readline/history.h"

void propagate_error(lua_State* state, int result)
{
        if (result != LUA_OK) {
                std::string message = lua_tostring(state, -1);
                lua_pop(state, 1);
                throw std::runtime_error{message};
        }
}

void dump_stack(lua_State* l)
{
        for (int n = 1; n <= lua_gettop(l); n++)
                std::cout << n << ". " << std::make_pair(l, n) << "\n";
        lua_pop(l, lua_gettop(l));
}

// The test application has three modes:

// If some arguments are provided, assume them to be file paths, and call
// luaL_loadfile() for each one.

// If stdin is a terminal, enter interactive mode, using readline to read
// commands and luaL_loadstring() to evaluate each one.

// Otherwise just read lines from stdin and pass them to luaL_loadstring()
// until something goes wrong.

void load_files(lua_State* state, int argc, char* argv[])
{
        for (int i = 1; i < argc; ++i) {
                ishtar::run(state, std::filesystem::path(argv[i]));
                dump_stack(state);
        }
}

void interactive(lua_State* state)
{
        using_history();
        for (;;) {
                auto line = readline(">> ");
                try {
                        ishtar::run(state, line);
                        dump_stack(state);
                } catch (std::exception& ex) {
                        std::cerr << "\e[0;31m" << ex.what() << "\e[0;0m\n";
                }
                add_history(line);
                free(line);
        }
}

void load_stdin(lua_State* state)
{
        ishtar::run(state, std::cin);
        dump_stack(state);
}

int main(int argc, char* argv[])
{
        try {
                lua_State* state = luaL_newstate();
                luaL_openlibs(state);
                test_openlibs(state);
                if (argc > 1)
                        load_files(state, argc, argv);
                else if (isatty(fileno(stdin)))
                        interactive(state);
                else
                        load_stdin(state);
                lua_close(state);
                return EXIT_SUCCESS;
        } catch (std::exception& ex) {
                std::cerr << "\e[0;31m" << ex.what() << "\e[0;0m\n";
                return EXIT_FAILURE;
        }
}
