
#include <sstream>
#include <memory>
#include <variant>
#include <vector>
#include <deque>
#include <list>
#include <set>
#include <unordered_set>
#include <map>
#include <unordered_map>

#include "ishtar.h"

// test functions with no arguments or return values
void foo() {}
struct Bar { void operator()() {} };

// test simple arguments and return values
int add(int x, int y) { return x + y; }
std::string cat(std::string x, std::string y) { return x + y; }

// test with simple arguments returning more than one value
std::pair<int, int> mydiv(int x, int y) { return std::make_pair(x / y, x % y); }

// test arguments of vector type
int accum(const std::vector<int>& x)
{
        int r = 0;
        for (auto i : x) r += i;
        return r;
}

std::vector<int> sort(std::vector<int> x)
{
        std::sort(x.begin(), x.end());
        return x;
}

std::vector<int> uniq(std::vector<int> x)
{
        x.erase(std::unique(x.begin(), x.end()), x.end());
        return x;
}

std::vector<int> rev(std::vector<int> x)
{
        std::reverse(x.begin(), x.end());
        return x;
}

// test raw function exporting
int equal(lua_State* state)
{
        if (lua_compare(state, 1, 2, LUA_OPEQ)) {
                lua_pushboolean(state, 1);
                return 1;
        }
        lua_pushnil(state);
        while (lua_next(state, 1)) {
                lua_pushvalue(state, -2);
                lua_gettable(state, 2);
                if (!lua_compare(state, -1, -2, LUA_OPEQ)) {
                        lua_pushboolean(state, 0);
                        return 1;
                }
                lua_pop(state, 2);
        }
        lua_pushboolean(state, 1);
        return 1;
}

// test arguments of variant type
std::variant<int, std::string> toggle(std::variant<int, std::string> x)
{
        if (std::holds_alternative<int>(x))
                return std::to_string(std::get<int>(x));
        if (std::holds_alternative<std::string>(x))
                return std::stoi(std::get<std::string>(x));
        return "what the hell?";
}

// test arguments of optional type
std::optional<std::string> maybe_str(std::optional<int> x)
{
        if (x) return std::to_string(x.value());
        return std::optional<std::string>{};
}

// test arguments of bitset type
std::bitset<8> byte(std::bitset<8> x)
{
        return x;
}

// test arguments of reference type
ishtar::Ref move(ishtar::Ref x) { return x; }

// test full class export
template<std::integral I> struct Vec {
        I x, y;
        Vec() = delete;
        Vec(I x, I y) : x{x}, y{y} {}
        std::pair<I, I> get() const { return std::make_pair(x, y); }
        Vec operator+(Vec r) { return Vec(x + r.x, y + r.y); }
        Vec operator-(Vec r) { return Vec(x - r.x, y - r.y); }
        Vec operator*(Vec r) { return Vec(x * r.x, y * r.y); }
        Vec operator/(Vec r) { return Vec(x / r.x, y / r.y); }
        Vec operator%(Vec r) { return Vec(x % r.x, y % r.y); }
        bool operator==(Vec r) { return x == r.x && y == r.y; }
};

using intv = Vec<int>;

// test full class export
template<std::integral I> struct Box {
        I l, r, u, d;
        Box() = delete;
        Box(I l, I r, I u, I d) : l{l}, r{r}, u{u}, d{d} {}
        std::tuple<I, I, I, I> get() const { return std::make_tuple(l, r, u, d); }
        Box operator+(Vec<I> v) { return Box(l + v.x, r + v.x, u + v.y, d + v.y); }
        Box operator-(Vec<I> v) { return Box(l - v.x, r - v.x, u - v.y, d - v.y); }
        Box operator*(Vec<I> v) { return Box(l * v.x, r * v.x, u * v.y, d * v.y); }
        Box operator/(Vec<I> v) { return Box(l / v.x, r / v.x, u / v.y, d / v.y); }
        Box operator%(Vec<I> v) { return Box(l % v.x, r % v.x, r % v.y, d % v.y); }
        bool operator==(Box b) { return l == b.l && r == b.r && u == b.u && d == b.d; }
};

using intb = Box<int>;

// test class autoexporting
struct Lazy { };

// test function object exporting
struct Baz {
        std::string operator()() { return "Yay!"; }
};

// test smart pointers
template<class T> std::optional<int> peek(const T& p)
{
        if (p) return *p.get();
        else return std::optional<int>{};
}

template<class T> void eat(T&& p) { T _{std::move(p)}; }

// test function returning itself
int world(lua_State* state)
{
        lua_pushcfunction(state, world);
        return 1;
}

// Test pointer exporting
int *external()
{
        static int i = 42;
        return &i;
}

// export everything
void test_openlibs(lua_State* l)
{
        static_assert(ishtar::function<void(*)()>);
        static_assert(ishtar::function<std::decay_t<decltype(foo)>>);
        static_assert(ishtar::function<std::decay_t<decltype(&Box<int>::get)>>);

        ishtar::globals(l)
                .set("containers", ishtar::table(l)
                        .set("vector", std::move<std::vector<int>>)
                        .set("deque", std::move<std::deque<int>>)
                        .set("list", std::move<std::list<int>>)
                        .set("set", std::move<std::set<int>>)
                        .set("mset", std::move<std::multiset<int>>)
                        .set("uset", std::move<std::unordered_set<int>>)
                        .set("umset", std::move<std::unordered_multiset<int>>)
                        .set("map", std::move<std::map<int, int>>)
                        .set("mmap", std::move<std::multimap<int, int>>)
                        .set("umap", std::move<std::unordered_map<int, int>>)
                        .set("ummap", std::move<std::unordered_multimap<int, int>>)

                        .set("sp", std::make_shared<int, int>)
                        .set("peek_sp", peek<std::shared_ptr<int>>)
                        .set("eat_sp", eat<std::shared_ptr<int>>)

                        .set("up", std::make_unique<int, int>)
                        .set("peek_up", peek<std::unique_ptr<int>>)
                        .set("eat_up", eat<std::unique_ptr<int>>)
                    )

                .set("world", world)

                .set("it", ishtar::table(l)
                        .set("foo", foo)
                        .set("bar", std::function<void()>(Bar{}))
                        .set("add", add)
                        .set("cat", cat)
                        .set("div", mydiv)
                        .set("move", move)
                        .set("accum", accum)
                        .set("uniq", uniq)
                        .set("sort", sort)
                        .set("rev", rev)
                        .set("equal", equal)
                        .set("toggle", toggle)
                        .set("maybe_str", maybe_str)
                        .set("byte", byte)
                        .set("lazy", std::function<Lazy()>([](){ return Lazy{}; }))
                        .set("external", external)
                    )

                .set("intv", ishtar::type<intv>(l)
                        .set_constructor<int, int>()
                        .set(ishtar::name, "intv")
                        .set(ishtar::add, &intv::operator+)
                        .set(ishtar::sub, &intv::operator-)
                        .set(ishtar::mul, &intv::operator*)
                        .set(ishtar::div, &intv::operator/)
                        .set(ishtar::mod, &intv::operator%)
                        .set(ishtar::eq, &intv::operator==)
                        .set("x", &intv::x)
                        .set("y", &intv::y)
                        .set("get", &intv::get)
                     )

                .set("intb", ishtar::type<intb>(l)
                        .set_constructor<int, int, int, int>()
                        .set(ishtar::name, "intb")
                        .set(ishtar::add, &intb::operator+)
                        .set(ishtar::sub, &intb::operator-)
                        .set(ishtar::mul, &intb::operator*)
                        .set(ishtar::div, &intb::operator/)
                        .set(ishtar::mod, &intb::operator%)
                        .set(ishtar::eq, &intb::operator==)
                        .set("l", &intb::l)
                        .set("r", &intb::r)
                        .set("u", &intb::u)
                        .set("d", &intb::d)
                        .set("get", &intb::get)
                     )

                .set("baz", ishtar::type<Baz>(l)
                        .set_constructor<>()
                        .set(ishtar::name, "baz")
                        .set(ishtar::call, &Baz::operator())
                    )
                ;
}
