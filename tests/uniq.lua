if (not it.equal(it.uniq({}), {})) then error("Bad table!") end
if (not it.equal(it.uniq({1}), {1})) then error("Bad table!") end
if (not it.equal(it.uniq({1,1}), {1})) then error("Bad table!") end
if (not it.equal(it.uniq({1,1,2}), {1,2})) then error("Bad table!") end
